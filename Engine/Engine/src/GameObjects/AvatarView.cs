﻿using System;
using Uniject;

namespace Engine
{
	[GameObjectBoundary]
	public class AvatarView : TestableComponent
	{
		private AvatarModel _model;

		public AvatarView (TestableGameObject obj, Character character, ILogger logger, AvatarModel model, IResourceLoader resourceLoader) : base (obj) {
			logger.Log ("AvatarView > init with: " + character.AvatarResourcePath);
			obj.name = "Avatar";

			var avatarGo = resourceLoader.instantiate (character.AvatarResourcePath);
			avatarGo.transform.Parent = Obj.transform;

			model.OnStrafeLeft += delegate() {
				
			};

			_model = model;
		}

		public override void Update () {
			base.Update ();
			Obj.transform.Position = _model.Position;
		}
	}
}

