﻿using System;
using Ninject.Modules;

namespace Engine
{
	public class GameInjection : NinjectModule
	{
		public override void Load () {
			Kernel.Bind<GameManager> ().ToSelf ().InSingletonScope ();
			Kernel.Bind<DataManager> ().ToSelf ().InSingletonScope ();
		}
	}
}

