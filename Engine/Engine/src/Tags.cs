﻿using System;

namespace Engine
{
	public class Tags
	{
		public const string Player = "Player";
		public const string Obstacle = "Obstacle";
		public const string Coin = "Coin";
		public const string Crystal = "Crystal";
		public const string Spawn = "Spawn";
	}
}

