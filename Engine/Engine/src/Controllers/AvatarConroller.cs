﻿using System;
using UnityEngine;
using Uniject;
using Ninject;

namespace Engine
{
	public class AvatarConroller {

		private AvatarModel _model;

		private IControlMethod _controlMethod;

		public AvatarConroller (IControlMethod controlMethod, AvatarModel model) {
			_model = model;
			_controlMethod = controlMethod;
		}

		public void Update() {
			if (_controlMethod.WantToStrafeLeft) {
				_model.StrafeLeft ();
			} else if (_controlMethod.WantToStrafeRight) {
				_model.StrafeRight ();
			} else if (_controlMethod.WantToJump) {
				_model.Jump ();
			} else if (_controlMethod.WantToDash) {
				_model.Dash ();
			}
			_model.Update ();
		}

	}
}

