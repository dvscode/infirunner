﻿using System;
using Uniject;

namespace Engine
{
	public class KeyboardContolMethod : TestableComponent, IControlMethod
	{
		public bool WantToStrafeLeft {
			get;
			private set;
		}
		public bool WantToStrafeRight {
			get;
			private set;
		}
		public bool WantToJump {
			get;
			private set;
		}
		public bool WantToDash {
			get;
			private set;
		}

		private IInput _input;

		public KeyboardContolMethod (TestableGameObject obj, IInput input) : base (obj) {
			obj.name = "KeyboardContolMethod";
			_input = input;
			WantToStrafeLeft = false;
			WantToStrafeRight = false;
			WantToJump = false;
			WantToDash = false;
		}

		public override void Update () {
			WantToStrafeLeft = _input.GetKey ("left") || _input.GetKey ("a");
			WantToStrafeRight = _input.GetKey ("right") || _input.GetKey ("d");
			WantToJump = _input.GetKey ("space") || _input.GetKey ("up") || _input.GetKey ("w");
			WantToDash = _input.GetKey ("down") || _input.GetKey ("s");
		}
	}
}

