﻿using System;

namespace Engine
{
	public interface IControlMethod {
		bool WantToStrafeLeft {
			get;
		}
		bool WantToStrafeRight {
			get;
		}
		bool WantToJump {
			get;
		}
		bool WantToDash {
			get;
		}
	}
}

