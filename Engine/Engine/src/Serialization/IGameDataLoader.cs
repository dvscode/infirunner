﻿using System;

namespace Engine
{
	public interface IGameDataLoader {
		GameData Load();
	}
}

