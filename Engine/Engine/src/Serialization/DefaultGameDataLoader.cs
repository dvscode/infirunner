﻿using System;
using System.IO;
using UnityEngine;

namespace Engine
{
	public class DefaultGameDataLoader : AbstractYamlLoader {

		const string DefaultGameData = @"---
characters:
- id: red
  speed: 3
  price: 0
  upgradeTiers: 3
  upgradeCost: 100
  avatarResourcePath: Avatars/red
  previewResourcePath: Preview/red
  
- id: green
  speed: 4
  price: 1000
  upgradeTiers: 3
  upgradeCost: 500
  avatarResourcePath: Avatars/green
  previewResourcePath: Preview/green
  
- id: blue
  speed: 5
  price: 5000
  upgradeTiers: 3
  upgradeCost: 1000
  avatarResourcePath: Avatars/blue
  previewResourcePath: Preview/blue

levels:
- id: level1
  name: Level 1
  sceneResourcePath: Level1

- id: level2
  name: Level 2
  sceneResourcePath: Level2
";

		public DefaultGameDataLoader () : base (new StringReader(DefaultGameData)) {
		}
		

	}
}

