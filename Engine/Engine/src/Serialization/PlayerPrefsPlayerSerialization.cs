﻿using System;
using UnityEngine;

namespace Engine
{
	public class PlayerPrefsPlayerSerialization : IPlayerSerialization {
		
		public Player LoadState() {
			var player = new Player ();
			player.Name = PlayerPrefs.GetString ("Player.Name", "");
			player.Money = PlayerPrefs.GetInt ("Player.Money", Defaults.StartingMoney);
			player.BestScore = PlayerPrefs.GetInt ("Player.BestScore", 0);
			return player;
		}

		public void SaveState(Player player) {
			PlayerPrefs.SetString ("Player.Name", player.Name);
			PlayerPrefs.SetInt ("Player.Money", player.Money);
			PlayerPrefs.SetInt ("Player.BestScore", player.BestScore);
			PlayerPrefs.Save ();
		}
	}
}

