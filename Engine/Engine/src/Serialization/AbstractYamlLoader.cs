﻿using System;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;
using System.IO;

namespace Engine {
	
	public abstract class AbstractYamlLoader : IGameDataLoader {

		private TextReader _textReader;

		public AbstractYamlLoader (TextReader reader) { 
			_textReader = reader;
		}

		public GameData Load() {
			var deserializer = new Deserializer (namingConvention: new CamelCaseNamingConvention ());
			try {
				var gameData = deserializer.Deserialize<GameData> (_textReader);
				return gameData;
			} catch (Exception ex) {
				throw new Exception ("AbstractYamlLoader > failed to parse text data: " + ex.Message, ex);
			}
		}
	}
}

