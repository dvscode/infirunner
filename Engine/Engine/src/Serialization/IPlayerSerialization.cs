﻿using System;

namespace Engine
{
	public interface IPlayerSerialization {
		Player LoadState();
		void SaveState(Player player);
	}
}

