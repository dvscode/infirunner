﻿using System;
using Engine;
using System.IO;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace Engine
{
	public class YamlPlayerSerialization : IPlayerSerialization {

		protected string _filePath;

		public YamlPlayerSerialization (string filePath) {
			_filePath = filePath;
		}

		public Player LoadState () {
			if (!File.Exists (_filePath)) {
				// nothing have been saved yet
				return CreateDefaultPlayer();
			}
			var reader = new StreamReader (_filePath);
			var deserializer = new Deserializer (namingConvention: new CamelCaseNamingConvention());
			var player = deserializer.Deserialize<Player> (reader);
			reader.Close ();
			return player;
		}

		public void SaveState (Player player) {
			var writer = new StreamWriter (_filePath);
			var serializer = new Serializer (namingConvention: new CamelCaseNamingConvention());
			serializer.Serialize (writer, player);
			writer.Close ();
		}

		Player CreateDefaultPlayer() {
			var player = new Player ();
			player.Name = "";
			player.Money = Defaults.StartingMoney;
			return player;
		}
	}
}

