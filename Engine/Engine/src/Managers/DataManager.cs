﻿using System;
using System.Collections.Generic;
using Ninject;

namespace Engine
{
	public class DataManager {

		private GameData _persistentData;

		private Player _player;
		public Player Player {
			get {
				return _player;
			}
			private set {
				_player = value;
			}
		}

		private IPlayerSerialization _playerSerializer;

		public List<Character> Characters {
			get {
				return _persistentData.Characters;
			}
		}

		public List<Level> Levels {
			get {
				return _persistentData.Levels;
			}
		}

		public DataManager (IKernel kernel, IGameDataLoader dataLoader, IPlayerSerialization playerSerializer) {
			_persistentData = dataLoader.Load ();
			_playerSerializer = playerSerializer;
			_player = _playerSerializer.LoadState ();
			kernel.Bind<Player> ().ToConstant (_player);
		}

		public void SetPlayerName (string name) {
			_player.Name = name;
			_playerSerializer.SaveState (_player);
		}

		public void SaveNewScore (int score) {
			if (_player.BestScore < score) {
				_player.BestScore = score;
				_playerSerializer.SaveState (_player);
			}
		}

		public void ChangeMoneyByAmount (int amount) {
			if (amount == 0)
				return;
			_player.Money += amount;
			_player.Money = _player.Money < 0 ? 0 : _player.Money;
			_playerSerializer.SaveState (_player);
		}

		public bool UpgradeCharacter (Character character) {
			// do we have enough money
			if (_player.Money >= character.UpgradeCost ) {
				// check if upgrade is allowed
				var upgrade = _player.GetCharacterUpgrade(character);
				if (upgrade == null || upgrade.UpgradeTier < character.UpgradeTiers) {
					_player.Money -= character.UpgradeCost;
					_player.AddCharacterUpgrade (character);
					_playerSerializer.SaveState (_player);
					return true;
				}
			}
			return false;
		}
	}

	public class GameData {
		public List<Character> Characters { get; set; }
		public List<Level> Levels { get; set; }

		public GameData() {
			Characters = new List<Character> ();
			Levels = new List<Level> ();
		}
	}
}

