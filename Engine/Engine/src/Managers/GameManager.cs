﻿using System;
using Uniject;
using Ninject;

namespace Engine
{
	public class GameManager
	{
		public enum EndGameType {
			Abort,
			Crashed,
			Complete
		}

		private Match _match;

		public GameManager () {
		}

		public void StartGame(Character character, Level level) {
			var kernel = UnityInjector.get ();

			//var characterArg = new Ninject.Parameters.ConstructorArgument ("character", character);
			//var levelArg = new Ninject.Parameters.ConstructorArgument ("level", level);
			//_match = kernel.Get<Match> (characterArg, levelArg);

			kernel.Bind<Character> ().ToConstant (character);
			kernel.Bind<Level> ().ToConstant (level);
			kernel.Bind<IControlMethod> ().To<KeyboardContolMethod> ();

			_match = kernel.Get<Match> ();
			kernel.Bind<Match> ().ToConstant (_match);

			_match.StartMatch ();
		}

		public void EndGame(EndGameType reason) {
			
		}

	}
}

