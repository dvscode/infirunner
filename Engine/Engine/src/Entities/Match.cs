﻿using System;
using Uniject;
using Ninject;

namespace Engine
{
	[GameObjectBoundary]
	public class Match : TestableComponent {

		public delegate void MatchEndDelegate (GameManager.EndGameType endReason);
		public event MatchEndDelegate OnMatchEnded;

		public delegate void LootChangedDelegate (int coins, int crystals);
		public event LootChangedDelegate OnLootChanged;

		public delegate void TimerChangedDelegate (float timer);
		public event TimerChangedDelegate OnTimerChanged;


		private ITime _time;
		private AvatarModel _avatarModel;
		//private AvatarView _avatarView;
		private AvatarConroller _avatarController;
		private World _world;

		private int _coinsCollected;
		private int _crystalsCollected;

		private float _matchStartTime;

		public Match (IKernel kernel, TestableGameObject obj, ITime time) : base (obj) {

			obj.name = "Match";

			_avatarModel = kernel.Get<AvatarModel> ();
			kernel.Bind<AvatarModel> ().ToConstant (_avatarModel);

			//_avatarView = 
			kernel.Get<AvatarView> ();
			_avatarController = kernel.Get<AvatarConroller> ();

			_world = kernel.Get<World> ();
			_time = time;

		}

		void HandleCrashed () {
			EndMatch (GameManager.EndGameType.Crashed);
		}

		void HandleLootPickup (Loot loot) {
			_coinsCollected += loot.Coins;
			_crystalsCollected += loot.Crystals;
			if (OnLootChanged != null) {
				OnLootChanged (_coinsCollected, _crystalsCollected);
			}
		}

		public void StartMatch() {
			_matchStartTime = _time.time;
			_avatarModel.OnPickedUpLoot += HandleLootPickup;
			_avatarModel.OnCrashed += HandleCrashed;
		}

		void EndMatch (GameManager.EndGameType endReason) {
			_avatarModel.OnPickedUpLoot -= HandleLootPickup;
			_avatarModel.OnCrashed -= HandleCrashed;
			if (OnMatchEnded != null) {
				OnMatchEnded (endReason);
			}
		}

		public override void Update () {
			base.Update ();
			_avatarController.Update ();
			_avatarModel.Update ();
			_world.UpdateChunks (_avatarModel.Position);

			if (_time.time - _matchStartTime >= Defaults.MatchTime) {
				EndMatch (GameManager.EndGameType.Complete);
			}

			if (OnTimerChanged != null) {
				OnTimerChanged (Timer);
			}
		}

		public int Crystals {
			get { 
				return _crystalsCollected;
			}
		}

		public int Coins {
			get {
				return _coinsCollected;
			}
		}

		public float Timer {
			get { 
				return Defaults.MatchTime - (_time.time - _matchStartTime); 
			}
		}
	}
}

