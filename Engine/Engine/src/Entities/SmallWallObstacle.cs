﻿using System;
using Uniject;

namespace Engine
{
	public class SmallWallObstacle : Obstacle
	{
		//public SmallWallObstacle (TestableGameObject obj, [Resource("Obstacles/small_wall")] TestableGameObject visualGo, AvatarModel model, ILogger logger) : base (obj, true, false, model, visualGo, logger) {
		public SmallWallObstacle ([Resource("Obstacles/small_wall")] TestableGameObject visualGo, AvatarModel model) : 
			base (true, false, model, visualGo) {
			
		}
	}
}

