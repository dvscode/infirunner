﻿using System;
using Uniject;
using Ninject;
using System.Collections.Generic;

namespace Engine
{
	[GameObjectBoundary]
	public class World : TestableComponent
	{
		private List<Chunk> _chunks;

		private UnityEngine.Vector3 _lastAvatarPosition;
		private IKernel _kernel;

		public World (TestableGameObject obj, Level level, ILogger logger, IResourceLoader loader, IKernel kernel) : base (obj) {
			logger.Log ("World > load level: " + level + " from " + level.SceneResourcePath);

			obj.name = "World";

			_kernel = kernel;

			// load scene data
			// TODO:
			// generate chunks
			_chunks = new List<Chunk>();
			for (int i = 0; i < Defaults.TotalChunks; i++) {
				var offsetArg = new Ninject.Parameters.ConstructorArgument ("chunkOffset", i);
				var chunk = kernel.Get<Chunk> (offsetArg);
				chunk.Obj.transform.Parent = obj.transform;
				_chunks.Add (chunk);
			}
		}

		public void UpdateChunks (UnityEngine.Vector3 avatarPosition) {
			// reduce update frequency
			if (avatarPosition.z - _lastAvatarPosition.z > 5f) {
				int minChunk = (int)Math.Round (avatarPosition.z / Chunk.ChunkLength) - 1;
				int maxChunk = minChunk + Defaults.TotalChunks;

				_chunks.FindAll (chunk => {
					return chunk.Obj.transform.Position.z < minChunk * Chunk.ChunkLength;
				}).ForEach (chunk => {
					chunk.Obj.Destroy ();
				});
					
				int removeCount = _chunks.RemoveAll (chunk => {
					return chunk.Obj.transform.Position.z < minChunk * Chunk.ChunkLength;
				});

				for (int idx = maxChunk - removeCount; idx < maxChunk; idx++) {
					var offsetArg = new Ninject.Parameters.ConstructorArgument ("chunkOffset", idx);
					var chunk = _kernel.Get<Chunk> (offsetArg);
					chunk.Obj.transform.Parent = Obj.transform;
					_chunks.Add (chunk);
				}
				_lastAvatarPosition = avatarPosition;
			}
		}
	}


	[GameObjectBoundary]
	public class Chunk : TestableComponent {
		
		private Type[] _obstacles = {
			typeof(SmallWallObstacle), typeof(MidWallObstacle), typeof(TallWallObstacle)
		};

		private Type[] _loot = {
			typeof(CoinLoot), typeof(CrystalLoot)
		};

		public const float ChunkLength = 20f;
		private const int ObstacleChance = 30;
		private const int LootChance = 10;
		private const int NothingChance = 60;

		private TestableGameObject _floor;

		public Chunk (TestableGameObject obj, [Resource("World/chunk_floor")] TestableGameObject chunkFloor, IKernel kernel, ILogger logger, int chunkOffset) : base (obj) {
			// add floor
			obj.name = "Chunk_" + chunkOffset;

			_floor = chunkFloor;
			_floor.transform.Parent = obj.transform;

			var random = new Random ();

			// generate obstacles
			for (int row = 0; row < ChunkLength; row++) {
				// skip each other row, and first 5 rows
				if (row % 2 == 0 || row + chunkOffset * ChunkLength < 10)
					continue;
				int spawnChance = random.Next (0, NothingChance + ObstacleChance + LootChance);
				Type[] spawnTypes = null;

				if (spawnChance < LootChance) {
					spawnTypes = _loot;
				} else if (spawnChance < ObstacleChance + LootChance) {
					spawnTypes = _obstacles;
				}

				if (spawnTypes != null) {
					var spawn = (TestableComponent) kernel.Get (spawnTypes [random.Next (0, spawnTypes.Length)]);
					spawn.Obj.transform.Parent = obj.transform;
					spawn.Obj.transform.Position = new UnityEngine.Vector3 (random.Next (0, Defaults.LaneCount), 0, row);
				}
			}

			obj.transform.Position = new UnityEngine.Vector3(0, 0, ChunkLength * chunkOffset);
		}
	}
}

