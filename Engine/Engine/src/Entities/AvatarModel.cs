﻿using System;
using UnityEngine;
using Uniject;

namespace Engine
{
	public class AvatarModel {



		private ITime _time;

		private Vector3 _position;
		public Vector3 Position {
			get {
				return _position;
			}

		}

		private int _lanePosition;
		private float _runSpeed;
		private float _jumpSpeed;
		private float _verticalSpeed;
		private float _strafeSpeed;

		public enum ActionState {
			Idle,
			Running,
			Strafing,
			Dashing,
			Jumping,
			Crashed
		}
		private ActionState _actionState;

		public delegate void AvatarModelEventDelegate();

		public event AvatarModelEventDelegate OnRun;
		public event AvatarModelEventDelegate OnStrafeLeft;
		public event AvatarModelEventDelegate OnStrafeRight;
		public event AvatarModelEventDelegate OnJump;
		public event AvatarModelEventDelegate OnDash;

		public event AvatarModelEventDelegate OnDidDashed;
		public event AvatarModelEventDelegate OnDidJumpOver;
		public event AvatarModelEventDelegate OnCrashed;

		public delegate void PickedUpLootDelegate(Loot loot);
		public event PickedUpLootDelegate OnPickedUpLoot;


		const float DashTimeout = 0.1f;
		const float JumpTimeout = 1f;
		const float StrageTimeout = 0.3f;

		const float StrafeSpeed = 6f;
		const float JumpSpeed = 6f;

		private float _actionStartTime;

		private ILogger _logger;

		public AvatarModel (ITime time, Character character, Player player, ILogger logger) {
			_runSpeed = player.CalculateCharacterFinalSpeed(character);
			_lanePosition = 1;
			_time = time;
			_actionState = ActionState.Running;
			_actionStartTime = 0;
			_position = new Vector3 (_lanePosition, 0, 0);
			_strafeSpeed = StrafeSpeed;
			_jumpSpeed = JumpSpeed;
			_verticalSpeed = 0;
			_logger = logger;
		}

		public void Update () {

			// check state timeouts
			switch (_actionState) {
			case ActionState.Strafing:
				if (_time.time - _actionStartTime >= StrageTimeout) {
					Run ();
				}
				Move();
				break;
			case ActionState.Dashing:
				if (_time.time - _actionStartTime >= DashTimeout) {
					Run ();
				}
				Move();
				break;
			case ActionState.Jumping:
				Move ();
				_verticalSpeed -= Defaults.Gravity * _time.DeltaTime;
				if (_position.y <= 0) {
					_position.y = 0;
					_verticalSpeed = 0;
					Run ();
				}
				break;
			case ActionState.Running:
				Move();
				break;

			case ActionState.Idle:
			case ActionState.Crashed:
				break;
			}
		}

		void Move() {
			_position.z += _runSpeed * _time.DeltaTime;
			_position.y += _verticalSpeed * _time.DeltaTime;
			_position.y = _position.y < 0 ? 0 : _position.y;
			_position.x = Mathf.Lerp (_position.x, _lanePosition, _time.DeltaTime * _strafeSpeed);
		}

		void Run() {
			if (_actionState != ActionState.Running) {
				_actionState = ActionState.Running;
				if (OnRun != null) {
					OnRun ();
				}

			}
		}

		public void StrafeLeft() {
			if (_lanePosition > 0 && _actionState == ActionState.Running) {
				_lanePosition--;
				_actionState = ActionState.Strafing;
				_actionStartTime = _time.time;
				if (OnStrafeLeft != null) {
					OnStrafeLeft ();
				}
			}
		}

		public void StrafeRight() {
			if (_lanePosition < Defaults.LaneCount - 1 && _actionState == ActionState.Running) {
				_lanePosition++;
				_actionState = ActionState.Strafing;
				_actionStartTime = _time.time;
				if (OnStrafeRight != null) {
					OnStrafeRight ();	
				}
			}
		}


		public void Dash() {
			if (_actionState == ActionState.Running) {
				_actionState = ActionState.Dashing;
				_actionStartTime = _time.time;
				if (OnDash != null) {
					OnDash ();
				}
			}
		}

		public void Jump() {
			if (_actionState == ActionState.Running) {
				_actionState = ActionState.Jumping;
				_actionStartTime = _time.time;
				_verticalSpeed = _jumpSpeed;
				if (OnJump != null) {
					OnJump ();
				}
			}
		}

		public void Crash() {
			_actionState = ActionState.Crashed;
			if (OnCrashed != null) {
				OnCrashed ();
			}
		}

		public void InteractWith (Obstacle obstacle) {
			if (obstacle.CanDash && _actionState == ActionState.Dashing) {
				_logger.Log ("AvatarModel > dashed obstacle");
				if (OnDidDashed != null) {
					OnDidDashed ();
				}
			} else if (obstacle.CanJumpOver && _actionState == ActionState.Jumping) {
				_logger.Log ("AvatarModel > jumped over obstacle");
				_verticalSpeed = Math.Max (_verticalSpeed, _jumpSpeed / 2);
				if (OnDidJumpOver != null) {
					OnDidJumpOver ();
				}
			} else {
				_logger.Log ("AvatarModel > crashed! " + obstacle + ", canJump: " + obstacle.CanJumpOver + ", state: " + _actionState + ", position: " + _position);

				Crash ();
			}
		}

		public void InteractWith (Loot loot) {
			_logger.Log ("AvatarModel > picked up loot: " + loot);
			if (OnPickedUpLoot != null) {
				OnPickedUpLoot (loot);	
			}
		}
	}
}

