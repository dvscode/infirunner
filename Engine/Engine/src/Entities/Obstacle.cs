﻿using System;
using Uniject;

namespace Engine
{
	public abstract class Obstacle : TestableComponent
	{
		public bool CanJumpOver {
			get;
			private set;
		}

		public bool CanDash {
			get;
			private set;
		}

		private AvatarModel _avatarModel;

		public Obstacle (bool canJumpOver, bool canDash, AvatarModel model, TestableGameObject visualGo) : base (visualGo) {
			CanDash = canDash;
			CanJumpOver = canJumpOver;
			//visualGo.transform.Parent = obj.transform;
			//visualGo.registerComponent (this);
			visualGo.name = "Obstacle_" + visualGo.name;
			_avatarModel = model;
		}

		public override void OnTriggerEnter (TestableGameObject other) {
			base.OnTriggerEnter (other);
			_avatarModel.InteractWith (this);
		}
	}
}

