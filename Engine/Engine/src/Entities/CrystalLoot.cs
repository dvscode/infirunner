﻿using System;
using Uniject;

namespace Engine
{
	public class CrystalLoot : Loot
	{
		public CrystalLoot (AvatarModel avatar, [Resource("Loot/crystal")] TestableGameObject visualGo) : base (0, 1, visualGo, avatar) {
			visualGo.name = "CrystalLoot";
		}
	}
}

