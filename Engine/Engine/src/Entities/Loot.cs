﻿using System;
using Uniject;

namespace Engine
{
	public class Loot  : TestableComponent {
		public int Coins {
			get; private set;
		}

		public int Crystals {
			get; private set;
		}

		private AvatarModel _avatarModel;

		public Loot (int coins, int crystals, TestableGameObject visualGo, AvatarModel avatarModel) : base (visualGo) {
			Coins = coins;
			Crystals = crystals;
			_avatarModel = avatarModel;
		}

		public override void OnTriggerEnter (TestableGameObject other) {
			base.OnTriggerEnter (other);
			_avatarModel.InteractWith (this);
			Obj.Destroy ();
		}
	}
}

