﻿using System;
using Uniject;

namespace Engine
{
	public class TallWallObstacle : Obstacle
	{
		public TallWallObstacle ([Resource("Obstacles/tall_wall")] TestableGameObject visualGo, AvatarModel model) : 
			base (false, false, model, visualGo) {

		}
	}
}
