﻿using System;
using Uniject;

namespace Engine
{
	public class MidWallObstacle : Obstacle
	{
		//public MidWallObstacle (TestableGameObject obj, [Resource("Obstacles/mid_wall")] TestableGameObject visualGo, AvatarModel model, ILogger logger) : base (obj, true, false, model, visualGo, logger) {
		public MidWallObstacle ([Resource("Obstacles/mid_wall")] TestableGameObject visualGo, AvatarModel model) : 
			base (false, false, model, visualGo) {
		}
	}
}
