﻿using System;
using Uniject;

namespace Engine
{
	public class CoinLoot : Loot
	{
		public CoinLoot (AvatarModel avatar, [Resource("Loot/coin")] TestableGameObject visualGo) 
			: base (1, 0, visualGo, avatar) {
			visualGo.name = "CoinLoot";
		}
	}
}

