﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace Engine
{
	public class Character
	{
		public string Id {
			get; set;
		}

		public float Speed {
			get; set;
		}

		public int Price {
			get; set;
		}

		public int UpgradeTiers {
			get; set;
		}

		public int UpgradeCost {
			get; set;
		}

		public string AvatarResourcePath {
			get; set;
		}

		public string PreviewResourcePath {
			get; set;
		}
	}
}

