﻿using System;

namespace Engine
{
	public class Level
	{
		public string Id {
			get;
			set;
		}

		public string Name {
			get;
			set;
		}

		public string SceneResourcePath {
			get;
			set;
		}
	}
}

