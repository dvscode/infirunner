﻿using System;
using System.Collections.Generic;

namespace Engine
{
	public class Player
	{
		public string Name {
			get; set;
		}

		public int BestScore {
			get; set;
		}

		public int Money {
			get; set;
		}

		public List<CharacterUpgrade> CharacterUpgrades {
			get;
			set;
		}

		public Player() {
			CharacterUpgrades = new List<CharacterUpgrade> ();
		}

		public CharacterUpgrade GetCharacterUpgrade (Character character) {
			return CharacterUpgrades.Find (match => { return match.CharacterId == character.Id; });
		}

		public float CalculateCharacterFinalSpeed (Character character) {
			var upgradeInfo = GetCharacterUpgrade (character);
			return character.Speed + (upgradeInfo != null ? upgradeInfo.UpgradeTier : 0);
		}

		public void AddCharacterUpgrade (Character character) {
			var upgrade = GetCharacterUpgrade (character);
			if (upgrade == null) {
				upgrade = new CharacterUpgrade () { CharacterId = character.Id, UpgradeTier = 0 };
				CharacterUpgrades.Add (upgrade);
			}
			if (upgrade.UpgradeTier < character.UpgradeTiers) {
				upgrade.UpgradeTier++;
			}
		}
	}

	public class CharacterUpgrade {
		public string CharacterId {
			get;
			set;
		}
		public int UpgradeTier {
			get;
			set;
		}
	}
}

