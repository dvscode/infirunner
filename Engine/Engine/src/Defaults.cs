﻿using System;

namespace Engine
{
	public class Defaults {
		public const int StartingMoney = 100;
		public const int MatchTime = 30;
		public const int LaneCount = 3;
		public const int TotalChunks = 5;
		public const float Gravity = 9.81f;
	}
}

