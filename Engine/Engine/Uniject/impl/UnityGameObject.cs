using System.Collections.Generic;
using System;
using UnityEngine;
using Uniject;

namespace Uniject.Impl {
    public class UnityGameObject : TestableGameObject {

        public GameObject obj { get; private set; }
        public UnityGameObject (GameObject obj) : base(new UnityTransform(obj)) {
            this.obj = obj;
            obj.AddComponent<UnityGameObjectBridge>().wrapping = this;
        }

        public override void Destroy() {
            base.Destroy();
            GameObject.Destroy (this.obj);
        }

		public override bool activeSelf {
			get { return obj.activeSelf; }
        }

		public override bool activeInHierarchy {
			get { return obj.activeInHierarchy; }
		}

        public override string name {
            get { return obj.name; }
            set { obj.name = value; }
        }

		public override void SetActive (bool value) {
			obj.SetActive (value);
		}

        public override int layer {
            get { return obj.layer; }
            set { obj.layer = value; }
        }
    }
}
