﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

	public Vector3 Offset;
	private Transform _target;

	void Start() {
		transform.rotation = Quaternion.identity;
	}

	// Update is called once per frame
	void LateUpdate () {
		if (_target == null) {
			var playerGo = GameObject.FindGameObjectWithTag (Engine.Tags.Player);
			if (playerGo != null) {
				_target = playerGo.transform;
			}
		}

		if (_target != null) {
			var position = Offset;
			position.z += _target.position.z;
			transform.position = position;
		}
	}
}
