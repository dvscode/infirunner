﻿using UnityEngine;
using System.Collections;
using Slash.Unity.DataBind.Core.Data;
using Engine;

public class MenuContext : Context {

	#region Fields

	/// <summary>
	///   Data binding property, used to get informed when a data change happened.
	/// </summary>
	private readonly Property<int> _crystals = new Property<int>();
	private readonly Property<int> _coins = new Property<int>();
	private readonly Property<float> _timer = new Property<float>();
	private Engine.Match _match;

	#endregion

	#region Constructors and Destructors

	DataManager _dataMgr;

	/// <summary>
	///   Constructor.
	/// </summary>
	public MenuContext (DataManager dataMgr) {
		Debug.Log (dataMgr.Characters.Count);

	}

	void HandleMatchEnded (Engine.GameManager.EndGameType endReason) {
		_match.OnLootChanged -= HandleLootChanged;
		_match.OnTimerChanged -= HandleTimerChanged;
		_match.OnMatchEnded -= HandleMatchEnded;
	}

	void HandleTimerChanged (float timer) {
		Timer = timer;
	}

	void HandleLootChanged (int coins, int crystals) {
		Crystals = crystals;
		Coins = coins;
	}

	#endregion

	#region Properties

	/// <summary>
	///   Crystalls collected
	/// </summary>
	public int Crystals {
		get {
			return _crystals.Value;
		}
		set {
			_crystals.Value = value;
		}
	}

	/// <summary>
	///   Coins collected
	/// </summary>
	public int Coins {
		get {
			return _coins.Value;
		}
		set {
			_coins.Value = value;
		}
	}

	/// <summary>
	///   Gameplay time left
	/// </summary>
	public float Timer {
		get {
			return _timer.Value;
		}
		set {
			_timer.Value = value;
		}
	}

	#endregion

	#region Public Methods and Operators

	/// <summary>
	///   Called when the OnChangeText command arrives.
	/// </summary>
	public void OnChangeText() {

	}

	#endregion
}
