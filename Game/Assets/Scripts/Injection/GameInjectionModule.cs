﻿using System;
using Ninject.Modules;
using Engine;

public class GameInjectionModule : NinjectModule
{
	public override void Load () {
		Kernel.Bind<IPlayerSerialization> ().To<PlayerPrefsPlayerSerialization> ();
		Kernel.Bind<IGameDataLoader> ().To<DefaultGameDataLoader> ();
		Kernel.Bind<DataManager> ().ToSelf ().InSingletonScope ();
	}
}
