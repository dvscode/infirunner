﻿using System;
using UnityEngine;
using Uniject;
using Ninject;

public class InjectContext : MonoBehaviour {

	public string ContextClass;

	void Start() {
		var kernel = UnityInjector.get ();
		kernel.Get (System.Type.GetType (ContextClass), new Ninject.Parameters.Parameter [] {});
	}
}

