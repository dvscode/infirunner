﻿using UnityEngine;
using System.Collections;
using Engine;
using Uniject;
using Ninject;

public class GameRootInjection : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
		var kernel = UnityInjector.get ();
		kernel.Load (new GameInjectionModule ());

		Application.LoadLevelAsync (Levels.MenuLevel);
	}
}
