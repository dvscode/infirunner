﻿using System;
using System.IO;
using Tests;
using Engine;
using Uniject;
using Ninject;
using Uniject.Impl;
using NUnit.Framework;

namespace EngineTest
{
	[TestFixture()]
	public class TestEngine : BaseInjectedTest {
		
		Exception _testFixtureSetupException = null;

		/*[TestFixtureSetUp]
		public void SetupEnvironment() {
			try {

				kernel.Bind<IGameDataLoader> ().To<TestGameData> ();
				kernel.Bind<IPlayerSerialization> ().To<TestPlayerSerialization> ();

				kernel.Get<GameRoot> ();

				//GameRoot.instance.Initialize (new TestGameData(), new YamlPlayerSerialization(playerDataPath));
			} catch (Exception ex) {
				_testFixtureSetupException = ex;
			}
		}

		[SetUp]
		// NUnit doesn't support very useful logging of failures from a TestFixtureSetUp method. We'll do the logging here.
		public void CheckForTestFixturefailure() { 
			if (_testFixtureSetupException != null)
			{
				string msg = string.Format("There was a failure during test fixture setup, resulting in a {1} exception. {0}Exception Message: {2}{0}Stack Trace:{3}",
					Environment.NewLine, _testFixtureSetupException.GetType(), _testFixtureSetupException.Message, _testFixtureSetupException.StackTrace);
				Assert.Fail(msg);
			}
		}*/

		[SetUp]
		public void GameSetup() {
			kernel.Bind<IGameDataLoader> ().To<TestGameData> ();
			kernel.Bind<IPlayerSerialization> ().To<TestPlayerSerialization> ();

			kernel.Get<GameRoot> ();
		}

		protected override IKernel createNewKernel () {
			return new StandardKernel(new UnityModule(), new TestModule(), new LateBoundModule(), new EngineTestModule ());
		}

		[Test()]
		public void TestPlayerDataMoney() {

			var player = kernel.Get<Player> ();

			int money = player.Money;
			kernel.Get<DataManager> ().ChangeMoneyByAmount (100);

			Assert.AreEqual (money + 100, player.Money);
		}

		[Test()]
		public void TestPlayerSaveLoad() {
			var tempPlayerDataPath = Path.Combine (Environment.CurrentDirectory, "playerDataTemp.yaml");

			if (File.Exists (tempPlayerDataPath)) {
				File.Delete (tempPlayerDataPath);
			}
				
			var serializer = new YamlPlayerSerialization (tempPlayerDataPath);

			// create temp player
			var player = new Player();
			player.Name = "Billy Jean";
			var random = new Random ();
			player.BestScore = random.Next ();
			player.Money = random.Next ();

			var character = kernel.Get<DataManager> ().Characters [0];
			player.AddCharacterUpgrade (character);

			serializer.SaveState (player);

			// load player from data
			var loadedPlayer = serializer.LoadState ();

			// compare
			Assert.AreEqual (player.Name, loadedPlayer.Name);
			Assert.AreEqual (player.Money, loadedPlayer.Money);
			Assert.AreEqual (player.BestScore , loadedPlayer.BestScore);
			Assert.AreEqual (player.GetCharacterUpgrade (character).UpgradeTier, loadedPlayer.GetCharacterUpgrade (character).UpgradeTier);

			File.Delete (tempPlayerDataPath);
		}

		[Test()]
		public void TestCharacterUpgrades() {
			var player = kernel.Get<Player> ();
			var character = kernel.Get<DataManager> ().Characters [0];

			// not enough money
			player.Money = character.UpgradeCost - 1;
			var upgradeResult = kernel.Get<DataManager> ().UpgradeCharacter (character);
			
			Assert.AreEqual (player.Money, character.UpgradeCost - 1); // money hasn't changed
			Assert.AreEqual (upgradeResult, false); // upgrade result was unsuccess
			Assert.IsNull ( player.GetCharacterUpgrade (character) ); // no character upgrade was added

			// successful upgrade
			player.Money = character.UpgradeCost;
			upgradeResult = kernel.Get<DataManager> ().UpgradeCharacter (character);
			var upgradeData = player.GetCharacterUpgrade (character);

			Assert.AreEqual (player.Money, 0); // no money left
			Assert.AreEqual (upgradeResult, true); // upgrade returned success
			Assert.NotNull ( upgradeData ); // something was written for character upgrade
			Assert.AreEqual ( upgradeData.UpgradeTier, 1); // upgrade tier is 1

			// max upgrade tier reached
			for (int i = 1; i < character.UpgradeTiers; i++) {
				player.Money = character.UpgradeCost;
				player.AddCharacterUpgrade (character);
			}
			upgradeData = player.GetCharacterUpgrade (character);
			Assert.AreEqual ( upgradeData.UpgradeTier, character.UpgradeTiers ); // reached max upgrade

			player.Money = character.UpgradeCost;
			upgradeResult = kernel.Get<DataManager> ().UpgradeCharacter (character);
			upgradeData = player.GetCharacterUpgrade (character);

			Assert.AreEqual (player.Money, character.UpgradeCost); // money weren't taken
			Assert.AreEqual ( upgradeResult, false); // unsuccess
			Assert.NotNull ( upgradeData ); // upgrade result should exist
			Assert.AreEqual ( upgradeData.UpgradeTier, character.UpgradeTiers ); // reached max upgrade
		}

		[Test()]
		public void TestStartMatch() {
			kernel.Bind<Character> ().ToConstant (kernel.Get<DataManager> ().Characters[0]);
			kernel.Bind<Level> ().ToConstant (kernel.Get<DataManager> ().Levels[0]);
			kernel.Bind<IControlMethod> ().To<KeyboardContolMethod> ();

			var match = kernel.Get<Match>();
			match.StartMatch ();
		}

		[Test()]
		public void TestStrafeLeft() {
			kernel.Bind<Character> ().ToConstant (kernel.Get<DataManager> ().Characters[0]);
			kernel.Bind<Level> ().ToConstant (kernel.Get<DataManager> ().Levels[0]);
			kernel.Bind<IControlMethod> ().To<KeyboardContolMethod> ();

			var match = kernel.Get<Match>();
			match.StartMatch ();

			var model = kernel.Get<AvatarModel> ();
			model.StrafeLeft ();
			step (1);
			Assert.AreEqual (0, model.Position.x);
		}

		[Test()]
		public void TestSingleton() {
			var player1 = kernel.Get<Player> ();
			var player2 = kernel.Get<Player> ();
			Assert.AreSame (player1, player2);
		}

	}
}

