﻿using System;
using System.IO;

namespace EngineTest
{
	public class TestPlayerSerialization : Engine.YamlPlayerSerialization
	{
		public const string yamlFileName = "playerData.yaml";

		public TestPlayerSerialization () : base (Path.Combine (Environment.CurrentDirectory, yamlFileName)) {
			if (File.Exists (_filePath)) {
				// start fresh
				File.Delete (_filePath);
			}

		}
	}
}

