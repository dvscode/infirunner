﻿using System;

namespace EngineTest
{
	public class TestGameData : Engine.AbstractYamlLoader
	{
		const string GameData = @"---
characters:
- id: red
  speed: 3
  price: 0
  upgradeTiers: 3
  upgradeCost: 100
  avatarResourcePath: Avatars/red
  previewResourcePath: Preview/red
  
- id: green
  speed: 4
  price: 1000
  upgradeTiers: 3
  upgradeCost: 500
  avatarResourcePath: Avatars/green
  previewResourcePath: Preview/green
  
- id: blue
  speed: 5
  price: 5000
  upgradeTiers: 3
  upgradeCost: 1000
  avatarResourcePath: Avatars/blue
  previewResourcePath: Preview/blue

levels:
- id: level1
  name: Level 1
  sceneResourcePath: Level1

- id: level2
  name: Level 2
  sceneResourcePath: Level2
";
		
		public TestGameData () : base (new System.IO.StringReader(GameData)) {
		}
	}
}

