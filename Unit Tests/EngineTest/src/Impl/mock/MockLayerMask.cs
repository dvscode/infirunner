using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace Tests {
    /// <summary>
    /// Makes reference to Unity's TagMangager to verify tag names.
    /// dvscode: Rewrote for Unity 5 & YAML parser
    /// </summary>
    public class MockLayerMask : ILayerMask {

        private Dictionary<string, int> layerMap = new Dictionary<string, int> ();

        public MockLayerMask() {

            /* Unity 4.6 variant, depreciated
            string[] lines = File.ReadAllLines("../../../../Game/ProjectSettings/TagManager.asset");
            var filtered = from l in lines
                where l.Contains("Layer") select l.Split(':')[1];

            filtered = from x in filtered select x.TrimStart(new char[] { ' ' });

            int count = 0;
            foreach (string layer in filtered) {
                layerMap[layer] = count++;
            }*/

			var inputDocument = File.ReadAllText("../../../../Game/ProjectSettings/TagManager.asset");
			var filtered = inputDocument.Substring(inputDocument.IndexOf("TagManager:"));
			var input = new StringReader(filtered);

			var deserializer = new Deserializer();
			//deserializer.RegisterTypeConverter
			//deserializer.RegisterTagMapping ("TagManager", typeof(TagManager));

			var document = deserializer.Deserialize<Document>(input);
			var tagManager = document.tagManager;
			for (int layerId = 0; layerId < tagManager.layers.Count; layerId++) {
				string layerName = tagManager.layers [layerId];
				if (layerName == null)
					continue;
				layerMap.Add (layerName, layerId);
			}
        }

        public int NameToLayer(string name) {
            return layerMap[name];
        }
    }

	public class Document {
		[YamlAlias("TagManager")]
		public TagManager tagManager { get; set; }
	}

	public class TagManager {
		public int serializedVersion { get; set; }
		public List<string> tags { get; set; }
		public List<string> layers { get; set; }
		public List<SortingLayer> m_SortingLayers { get; set; }
	}

	public class SortingLayer {
		public string name { get; set; }
		public int uniqueID { get; set; }
		public int locked { get; set; }
	}
}

